# homeos-repo

Generate git-repo tool manifest (`default.xml`) from GitLab API accessible non-personal projects (see https://gerrit.googlesource.com/git-repo/).

This is work-in-progress.

## Generate repo-tool manifest

### Create GitLab personal access token

To generate the `default.xml` project list, you have to first create a GitLab Personal Access Token (PAT) at https://gitlab.com/-/profile/personal_access_tokens with scope `read_api`.

To exclude certain projects, add them to the `generate/blacklist.txt` file in the form `group/project default_branch`, i.e. `DEMO/test01 master`

### Provide token and environment, run generation

```
export GITLAB_PERSONAL_ACCESS_TOKEN="<YOUR PAT>"
export GITLAB_API_PATH="<PATH TO GITLAB REST API V4>" # default https://gitlab.com/api/v4
export GITLAB_REPO_BASE_PATH="<PATH TO GITLAB IN WHICH THE PROJECTS RESIDE>" # default https://gitlab.com
export CERTIFICATES_DIR="<PATH TO DIR CONTAINING userkey.pem AND user.crt>" # i.e. /home/user/projects/workbench-gitlab - default unset (no need to set if no specific certificates are required)

chmod u+rx generate_manifest.sh
./generate_manifest.sh
```

---
**⚠ NOTE**

If using public GitLab, you will retrieve a list of thousands/millions of project repositories (exceeding your API quota in the process) - this is most probably not what you want.

This script is intended either for self-hosted GitLab instances, or for adding additional custom filter criteria to the GitLab API call in `.generate_manifest.sh`

---

## Use repo-tool manifest

Now that the repo-tool manifest containing the project list is generated, you can use it as described at https://gerrit.googlesource.com/git-repo/ and https://source.android.com/setup/develop/repo, i.e.

after uploading the generated `default.xml` manifest file to a git repository, you can use it

```
./repo init -u https://<git-repository-path-with-default-xml>.git
./repo sync
```

Now you should have all to you visible, non-personal projects checked out with their respective default branches inside the working directory.

Non-personal meaning no projects that reside in a personal GitLab "user" group.
