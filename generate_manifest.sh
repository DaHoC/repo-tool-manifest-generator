#!/usr/bin/env bash
mkdir -p out
rm -rf out/*
cd out || exit;
singleRequestProjectCount=-1;
currentPageNumber=1;
# Set defaults
if [ -z "${GITLAB_API_PATH}" ]; then
  export GITLAB_API_PATH="https://gitlab.com/api/v4"
fi
if [ -z "${GITLAB_REPO_BASE_PATH}" ]; then
  export GITLAB_REPO_BASE_PATH="https://gitlab.com"
fi

# Request projects, max 100 per request/page and collect
while [[ $singleRequestProjectCount -ne 0 ]]; do
  singleRequestFileName="response${currentPageNumber}.json"
  if [ -z "${CERTIFICATES_DIR}" ]; then
    curl -X GET -k -L --silent -S "${GITLAB_API_PATH}/projects/?simple=true&per_page=100&page=${currentPageNumber}" -H "private-token: ${GITLAB_PERSONAL_ACCESS_TOKEN}" -o ${singleRequestFileName};
  else
    curl -X GET --key "${CERTIFICATES_DIR}"/userkey.pem --cert "${CERTIFICATES_DIR}"/user.crt -k -L --silent -S "${GITLAB_API_PATH}/projects/?simple=true&per_page=100&page=${currentPageNumber}" -H "private-token: ${GITLAB_PERSONAL_ACCESS_TOKEN}" -o ${singleRequestFileName};
  fi
  singleRequestProjectCount=$(jq length ${singleRequestFileName})
  echo "Returned ${singleRequestProjectCount} entries for request ${currentPageNumber}"
  currentPageNumber=$((currentPageNumber+1))
done;
# Merge response JSON collections
jq -s '[ .[] | group_by(.id)[][] ]' response*.json > responses.json
overallProjectCount=$(jq length responses.json)
echo "Returned ${overallProjectCount} entries"
jq -r '.[] | select( .namespace.kind != "user" ) | "\(.path_with_namespace) \(.default_branch)"' responses.json | sort -u > projectlist.txt;
# Here projectlist and blacklist need to be already sorted
diff --new-line-format="" --unchanged-line-format="" <(sort projectlist.txt) <(sort ../generate/blacklist.txt) > projectlist_filtered.txt;
while read -r line; do echo "<project name=\"$(echo "${line}" | cut -d' ' -f1).git\" path=\"$(echo "${line}" | cut -d' ' -f1)\" revision=\"$(echo "${line}" | cut -d' ' -f2)\"/>" >> projectlist.xml; done < projectlist_filtered.txt
sed -i 's/^/  /' projectlist.xml;
sed $'/<\/manifest>/{e cat projectlist.xml\n}' ../generate/manifest_template.xml > default.xml;
sed -i "s/__PLACE__/${GITLAB_REPO_BASE_PATH}/" default.xml
